import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Register(){

    // State hooks to store the values of the input fields
    // getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
    const [isRegistered, setIsRegistered] = useState(false);

    const { user } = useContext(UserContext);



    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

        fetch('http://localhost:4000/users/checkEmail', {
            method: 'POST',
            headers: {
            	'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
        })
        .then(res => res.json())
        .then(emailExists => {

            if(!emailExists){
                fetch('http://localhost:4000/users/register', 
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            mobileNo: mobileNo,
                            email: email,
                            password: password1
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        console.log(data)
                        setIsRegistered(data)
                        if(data){
                            Swal.fire({
                                title: 'Registration successful',
                                icon: 'success',
                                text: 'Welcome to Zuitt!'
                            })
                            
                        } else {
                            Swal.fire({
                                title: 'Registration Failed',
                                icon: 'error',
                                text: 'Please use correct input.'
            
                            })
                        }                  
                    })
                
            } else {
                
                Swal.fire({
					title: 'Duplicate Email',
					icon: 'error',
					text: 'Please use different email.'

				})
            }
        })
        
        // Clear input fields

        setFirstName("");
        setLastName("");
        setMobileNo("");
        setEmail("");
        setPassword1("");
        setPassword2("");   
    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !=='') && (password1 === password2) && (mobileNo.length == 11)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, mobileNo, email, password1, password2]);

    return(
        ( isRegistered) ?
			<Navigate to='/login'/>
		: 

            // 2-way Binding (Binding the user input states into their corresponding input fields via the onChange JSX event Handler)
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group className='mb-3' controlId='userFirstName'>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type='first name' 
                        placeholder='Enter first name'
                        value={ firstName }
                        onChange={ e => setFirstName(e.target.value)} 
                        required/>
                </Form.Group>

                <Form.Group className='mb-3' controlId='userLastName'>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type='last name' 
                        placeholder='Enter last name'
                        value={ lastName }
                        onChange={ e => setLastName(e.target.value)} 
                        required/>
                </Form.Group>

                <Form.Group className='mb-3' controlId='userMobileNumber'>
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                        type='mobile number' 
                        placeholder='Enter mobile number'
                        value={ mobileNo }
                        onChange={ e => setMobileNo(e.target.value)} 
                        required/>
                </Form.Group>

                <Form.Group className='mb-3' controlId='userEmail'>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type='email' 
                        placeholder='Enter email'
                        value={ email }
                        onChange={ e => setEmail(e.target.value)} 
                        required/>
                    <Form.Text className='text-muted'>
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className='mb-3' controlId='password1'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type='password' 
                        placeholder='Password'
                        value={ password1 }
                        onChange={ e => setPassword1(e.target.value)} 
                        required/>
                </Form.Group>

                <Form.Group className='mb-3' controlId='password2'>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type='password' 
                        placeholder='Verify Password'
                        value={ password2 }
                        onChange={ e => setPassword2(e.target.value)}
                        required/>
                </Form.Group>

                { isActive ?
                    <Button variant='primary' type='submit' id='submitBtn'>
                        Submit
                    </Button>
                    :
                    <Button variant='danger' type='submit' id='submitBtn' disabled>
                        Submit
                    </Button>
                }
            </Form>
    )
};